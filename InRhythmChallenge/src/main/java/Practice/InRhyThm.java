package Practice;

import java.util.HashMap;

/**
 * Hello world!
 */
public class InRhyThm {

    public static final String LONGEST_WORD_LENGHT_KEY = "Longest word Length";
    public static final String LONGEST_WORD_KEY = "Longest word";

    public static HashMap<String, String> LongestWordLength(String str) {
        HashMap<String, String> returnHashMap = new HashMap<>();
        int length = 0;
        String longestWord = null;

        if (str.trim().length() > 0) {
            String[] words = str.split(" ");
            for (String word : words) {
                if (length < word.length()) {
                    length = word.length();
                    longestWord = word;
                }
            }
            returnHashMap.put(LONGEST_WORD_LENGHT_KEY, String.valueOf(length));
            returnHashMap.put(LONGEST_WORD_KEY, longestWord);
        } else {
            returnHashMap.put(LONGEST_WORD_LENGHT_KEY, "0");
            returnHashMap.put(LONGEST_WORD_KEY, "");
        }
        return returnHashMap;
}

}