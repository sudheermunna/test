package Practice;

import static org.junit.Assert.assertTrue;

import org.junit.Assert;
import org.junit.Test;

/**
 * Unit test for simple InRhyThm.
 */
public class InRhyThmTest
{
    /**
     * Rigorous Test :-)
     */

    private static final String TEST_INPUT_1 = "Life is really simple, but we insist on making it complicated";
    private static final String TEST_INPUT_2 = "lorik lorik lorik lorik lorik lorik app";
    private static final String TEST_INPUT_3 = "The cow jumped over the moon";
    private static final String TEST_INPUT_4 = "  ";

    @Test
    public void shouldAnswerWithTrue()
    {

        Assert.assertTrue("FAIL: Expected result is false", Integer
                .valueOf(InRhyThm.LongestWordLength(TEST_INPUT_1).get(InRhyThm.LONGEST_WORD_LENGHT_KEY))==11);
        Assert.assertTrue(InRhyThm.LongestWordLength(TEST_INPUT_1)
                .get(InRhyThm.LONGEST_WORD_KEY).equals("complicated"));

        Assert.assertTrue("FAIL: Expected result is false", Integer
                .valueOf(InRhyThm.LongestWordLength(TEST_INPUT_2).get(InRhyThm.LONGEST_WORD_LENGHT_KEY))==5);
        Assert.assertTrue(InRhyThm.LongestWordLength(TEST_INPUT_2)
                .get(InRhyThm.LONGEST_WORD_KEY).equals("lorik"));

        Assert.assertTrue("FAIL: Expected result is false", Integer
                .valueOf(InRhyThm.LongestWordLength(TEST_INPUT_3).get(InRhyThm.LONGEST_WORD_LENGHT_KEY))==6);
        Assert.assertTrue(InRhyThm.LongestWordLength(TEST_INPUT_3)
                .get(InRhyThm.LONGEST_WORD_KEY).equals("jumped"));

        Assert.assertTrue("FAIL: Expected result is false", Integer
                .valueOf(InRhyThm.LongestWordLength(TEST_INPUT_4).get(InRhyThm.LONGEST_WORD_LENGHT_KEY))==0);
        Assert.assertTrue(InRhyThm.LongestWordLength(TEST_INPUT_4)
                .get(InRhyThm.LONGEST_WORD_KEY).isEmpty());
    }
}
